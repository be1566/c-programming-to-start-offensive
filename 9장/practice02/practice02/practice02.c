#include <stdio.h>

int main(void)
{
	int aList[5][5] = { 0 };
	int nCount = 0;
	int x = 5, y = 0, nLength = 9, nDirection = -1;

	for (nLength = 9; nLength > 0; nLength -= 2)
	{
		for (int i = 0; i < nLength; ++i)
		{
			if (i < nLength / 2 + 1)	x += nDirection;
			else						y -= nDirection;

			aList[y][x] = ++nCount;
		}
		nDirection = -nDirection;
	}

	for (int i = 0; i < 5; ++i)
	{
		for (int k = 0; k < 5; ++k)
			printf("%d\t", aList[i][k]);
		putchar('\n');
	}

	return 0;
}