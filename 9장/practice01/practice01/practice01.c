#include <stdio.h>

int main(void)
{
	int aList[5][5] = { 0 };
	int nCount = 0, flag = 1;

	for (int i = 0; i < 5; ++i)
	{
		if (flag)
		{
			for (int k = 0; k < 5; ++k)
				aList[k][i] = ++nCount;
			flag = 0;
		}
		else
		{
			for (int k = 4; k >= 0; --k)
				aList[k][i] = ++nCount;
			flag = 1;
		}
	}

	for (int i = 0; i < 5; ++i)
	{
		for (int k = 0; k < 5; ++k)
			printf("%d\t", aList[i][k]);

		putchar('\n');
	}
	return 0;
}