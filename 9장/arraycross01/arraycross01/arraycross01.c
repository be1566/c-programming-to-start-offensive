#include <stdio.h>

int main(void)
{
	int nList[5][5] = { 0 };
	int nCounter = 1;

	for (int i = 0; i < 5; ++i)
	{
		if (i % 2 == 0)
			for (int k = 0; k < 5; ++k)
			{
				nList[i][k] = nCounter;
				nCounter++;
			}
		else
			for (int k = 0; k < 5; ++k)
			{
				nList[i][4 - k] = nCounter;
				nCounter++;
			}
	}

	for (int i = 0; i < 5; ++i)
	{
		for (int k = 0; k < 5; ++k)
			printf("%d\t", nList[i][k]);
		putchar('\n');
	}

	return 0;
}