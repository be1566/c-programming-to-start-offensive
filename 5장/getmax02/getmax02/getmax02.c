#include <stdio.h>

int main(void)
{
	int nMax = 0;
	int a, b, c;

	// 여기에 코드를 삽입하세요.
	scanf("%d %d %d", &a, &b, &c);

	nMax = a;
	nMax = nMax > b ? nMax : b;
	nMax = nMax > c ? nMax : c;

	printf("MAX : %d\n", nMax);
	return 0;
}