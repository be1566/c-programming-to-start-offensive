#include <stdio.h>
#include <stdlib.h>

void TestFunc1(int nParam)
{
	puts("TestFunc1");
}

void TestFunc2(int nParam)
{
	puts("TestFunc2");
}

void TestFunc3(int nParam)
{
	puts("TestFunc3");
}

int main(void)
{
	// 함수 포인터의 배열... 함수 각각을 배열에 집어넣을 수 있다...
	void(*pfList[3])(int) = {
		TestFunc1, TestFunc2, TestFunc3
	};
	
	int nInput;
	scanf_s("%d", &nInput);

	pfList[1](10);

	return 0;
}