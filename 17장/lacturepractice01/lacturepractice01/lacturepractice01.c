#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

// 각 항을 비교하는 함수
int MyCmp(const void *left, const void *right)
{
	// void Pointer 이기에 간접지정해줘야 한다.
	// return *(int*)left - *(int*)right;
	return strcmp(*(char**)left, *(char**)right);
}

int main(void)
{
	char *aList[5] = {
		"Apple",
		"Samsung",
		"LG",
		"HUAWEI",
		"HTC"
	};

	qsort(aList, 5, sizeof(char*), MyCmp);

	for (int i = 0; i < 5; ++i)
		printf("%s\n", aList[i]);
	return 0;
}