#include <stdio.h>

int main(void)
{
	int nAge = 0;
	printf("나이를 입력하세요. : ");
	scanf("%d", &nAge);	//뒤에 %*c를 입력하면 \n를 무시할 수 있다.

	fseek(stdin, 0, SEEK_END);

	char szName[32] = { 0 };
	printf("이름을 입력하세요 : ");
	gets(szName);

	printf("당신의 나이는 %d살이고 이름은 \'%s\'입니다.\n", nAge, szName);
	return 0;
}