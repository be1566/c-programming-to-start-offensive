#include <stdio.h>

int main(void) {
	char szBuffer[100] = { 0 };
	int nLength = 0;

	gets(szBuffer);
	while (szBuffer[nLength] != '\0')
		nLength++;

	printf("한글 문자의 개수는 %d자 입니다.\n", nLength / 2);

	return 0;
}