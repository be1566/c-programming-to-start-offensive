#include <stdio.h>

int main(void) {
	int nList[5] = { 0 };
	int nMin, nMax;

	scanf("%d %d %d %d %d", &nList[0], &nList[1], &nList[2], &nList[3], &nList[4]);

	nMax = nList[0];
	nMin = nList[0];
	for (int i = 0; i < 5; i++) {
		if (nList[i] > nMax)
			nMax = nList[i];
		if (nList[i] < nMin)
			nMin = nList[i];
	}

	printf("MIN: %d, MAX: %d\n", nMin, nMax);
	return 0;
}