#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
	int nPlayer = 0, nComputer = 0;
	
	puts("가위 바위 보 게임");

	printf("가위(0), 바위(1), 보(2)중 하나를 입력하세요 : ");
	scanf("%d", &nPlayer);

	while (nPlayer < 0 || nPlayer > 2)
	{
		printf("잘못된 입력입니다.\n");
		printf("가위(0), 바위(1), 보(2)중 하나를 입력하세요 : ");
		scanf("%d", &nPlayer);
	}

	srand(time(0));
	nComputer = rand() % 3;

	printf("사용자 : %d\n", nPlayer);
	printf("컴퓨터 : %d\n", nComputer);

	if (nPlayer == nComputer)
		puts("무승부\n");
	else
	{
		if (nPlayer == 0)
		{
			if (nComputer == 1)
				puts("패배\n");
			else
				puts("승리\n");
		}
		if (nPlayer == 1)
		{
			if (nComputer == 0)
				puts("승리\n");
			else
				puts("패배\n");
		}
		if (nPlayer == 2)
		{
			if (nComputer == 0)
				puts("패배\n");
			else
				puts("승리\n");
		}
	}
	return 0;
}