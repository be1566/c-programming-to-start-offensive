#include <stdio.h>

char* MyStrcpy(char *dst, int nSize, char *src)
{
	for (int i = 0; i < nSize; ++i)
		dst[i] = src[i];

	return dst;
}

int main(int argc, char* argv[])
{
	char szBufferSrc[12] = { "TestString" };
	char szBufferDst[12] = { 0 };

	MyStrcpy(szBufferDst, sizeof(szBufferDst), szBufferSrc);
	puts(szBufferDst);
	return 0;
}