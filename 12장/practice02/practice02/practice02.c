#include <stdio.h>
#include <string.h>

void SortString(char **paList, int nSize)
{
	int nMin;

	for (int i = 0; i < nSize - 1; ++i)
	{
		nMin = i;
		for (int k = i + 1; k < nSize; ++k)
		{
			if (strcmp(paList[nMin], paList[k]) > 0)
				nMin = k;
		}
		if (nMin != i)
		{
			char *pszTemp = paList[nMin];
			paList[nMin] = paList[i];
			paList[i] = pszTemp;
		}
	}
}

int main(void)
{
	char* aList[5] = {
		"������",
		"��ȫö",
		"����",
		"���缮",
		"�ڸ���"
	};
	int i = 0;

	SortString(aList, 5);

	for (i = 0; i < 5; ++i)
		puts(aList[i]);
	return 0;
}