#include <stdio.h>
#include <string.h>

int MyStrCmp(char *str1, char *str2)
{
	if (strlen(str1) == strlen(str2))
	{
		for (int i = 0; i < strlen(str1); ++i)
			if (!(str1[i] - str2[i] == 0 || str1[i] - str2[i] == 32 || str1[i] - str2[i] == -32))
				return 1;
		return 0;
	}
	return 1;
}

int main(void)
{
	printf("%d\n", MyStrCmp("Hello", "hello"));
	return 0;
}