#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void printDay(time_t t)
{
	struct tm *time = { 0 };
	time = localtime(&t);

	printf("%04d-%02d-%02d\n", time->tm_year + 1900, time->tm_mon + 1, time->tm_mday);
}

int main(void)
{
	time_t today;
	today = time(NULL);

	printDay(today);
	printDay(today + 60 * 60 * 24 * 10);
	printDay(today + 60 * 60 * 24 * 100);

	return 0;
}