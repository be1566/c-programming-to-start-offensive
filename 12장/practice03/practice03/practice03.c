#include <stdio.h>
#include <stdlib.h>

int GetTotal(int (*nArray)[4])
{
	int nTotal = 0;

	for (int i = 0; i < 5; ++i)
		for (int k = 0; k < 4; ++k)
			nTotal += nArray[i][k];

	return nTotal;
}

int main(void)
{
	int nArray[5][4] = {
		0, 1, 2, 3,
		4, 5, 6, 7,
		8, 9, 10, 11,
		12, 13, 14, 15,
		16, 17, 18, 19
	};
	
	printf("����� �� �� : %d\n", GetTotal(nArray));

	return 0;
}