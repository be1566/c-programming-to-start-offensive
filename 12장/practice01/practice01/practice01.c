#include <stdio.h>
#include <string.h>

int mystrstr(const char *strSet, const char *strSearch)
{
	char *buffer = strSet;
	int nIndex = 0;

	while (*buffer != NULL)
	{
		if (*buffer == *strSearch)
		{
			for (int i = 0; i < strlen(strSearch); ++i)
			{
				if (strSet[i] != strSearch[i])
					return -1;
			}
			nIndex = buffer - strSet;
			return nIndex;
		}
		buffer++;
	}
	return -1;
}

int main(void)
{
	printf("%d\n", mystrstr("Hello C Programmer", "Hel"));
	return 0;
}