/* 1) 표준입력장치(stdin)에서 문자열을 입력받은 후, 표준 출력장치(stdout)로 출력하는 프로그램을 작성하세요
	  단, 반드시 fgets()와 fputs() 함수를 사용하세요. */
#include <stdio.h>

int main()
{
	char *szBuffer[100] = { 0 };
	fgets(szBuffer, sizeof(szBuffer), stdin);
	fputs(szBuffer, stdout);
	return 0;
}