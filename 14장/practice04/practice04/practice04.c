/* 4) main() 함수의 인자로 두 바이너리 파일의 경로를 입력받아 바이너리 모드로 열고,
	  원본 파일의 내용을 읽어들여 대상 파일로 복사하는 프로그램을 작성하세요.
	  단, 복사의 진행 과정을 백분율로 화면에 출력하고, 한 번에 4KB 단위로 복사합니다.
	  그리고 대상 파일이 존재 유무에 상관없이 무조건 생성하며, 파일의 크기가 최대 100MB 이상인 경우는 고려하지 않는다. */
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
	FILE *src = NULL;
	FILE *dst = NULL;
	char szBuffer[4096] = { 0 };
	char fileName1[128] = { 0 }, fileName2[128] = { 0 };
	int fileSize = 0, count = 0;

	printf("복사할 파일의 이름을 입력하세요 : ");
	gets_s(fileName1, sizeof(fileName1));
	printf("새로 생성할 파일의 이름을 입력하세요 : ");
	gets_s(fileName2, sizeof(fileName2));

	src = fopen(fileName1, "rb");
	if (src == NULL)
		return;
	fseek(src, 0, SEEK_END);
	fileSize = ftell(src);
	fseek(src, 0, SEEK_SET);
	dst = fopen(fileName2, "wb");

	while (fread(szBuffer, sizeof(szBuffer), 1, src))
	{
		fwrite(szBuffer, sizeof(szBuffer), 1, dst);
		memset(szBuffer, 0, sizeof(szBuffer));
		count++;

		printf("복사중...[%.2f%%]\n", (double)409600.0 / fileSize * count);
	}
	puts("복사가 완료되었습니다.");

	_fcloseall();

	return 0;
}