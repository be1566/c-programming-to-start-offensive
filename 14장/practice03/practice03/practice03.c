/* 3) 현재 경로에서 'TestFile.txt' 텍스트 파일을 연 후, 원본 파일에서 행 단위로 문자열을 읽어 들여 대상 파일에 복사하는 프로그램을 작성하세요.
	  단, 대상 파일도 현재 경로에 생성하며, 파일명은 'DstFile.txt'로 합니다.	 */
#include <stdio.h>

int main(void)
{
	FILE *sFp = NULL, *dFp = NULL;
	char szBuffer[512] = { 0 };
	
	sFp = fopen("TestFile.txt", "r");
	if (sFp == NULL)
		return 1;

	dFp = fopen("DstFile.txt", "w");
	if (dFp == NULL)
		return 1;

	while (fgets(szBuffer, sizeof(szBuffer), sFp))
		fputs(szBuffer, dFp);

	fclose(sFp);
	fclose(dFp);

	return 0;
}