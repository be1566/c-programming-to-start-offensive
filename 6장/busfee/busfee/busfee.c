#include <stdio.h>

int main(void)
{
	int nfee = 1000, nAge;

	scanf("%d", &nAge);

	if (nAge < 20)
		nfee -= (int)(nfee * 0.25);

	printf("최종요금: %d원\n", nfee);

	return 0;
}