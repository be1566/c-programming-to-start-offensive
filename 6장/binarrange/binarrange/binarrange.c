#include <stdio.h>

int main(void)
{
	int nFee = 1000, nAge;

	scanf("%d", &nAge);

	if (nAge > 13)
	{
		if (nAge >= 14 && nAge <= 19)
			nFee -= (int)(nFee * 0.25);
	}
	else
	{
		if (nAge >= 4 && nAge <= 13)
			nFee -= (int)(nFee * 0.5);
		else
			nFee = 0;
	}

	printf("최종요금: %d원\n", nFee);
	return 0;
}