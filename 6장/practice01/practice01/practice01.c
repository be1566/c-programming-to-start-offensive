#include <stdio.h>

int main(void)
{
	int nInput, nMax = -1;

	for (int i = 0; i < 5; i++)
	{
		scanf("%d", &nInput);

		if (nInput < 0)
			nInput = 0;
		else if (nInput > 100)
			nInput = 100;

		if (nMax < nInput)
			nMax = nInput;
	}

	printf("%d\n", nMax);

	return 0;
}