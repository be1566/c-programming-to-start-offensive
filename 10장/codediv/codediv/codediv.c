#include <stdio.h>

int GetData(void)
{
	int nInput;

	printf("정수를 입력하세요. : ");
	scanf("%d", &nInput);

	return nInput;
}

int GetMax(int nData1, int nData2, int nData3)
{
	int nMax = nData1;
	if (nData2 > nMax) nMax = nData2;
	if (nData3 > nMax) nMax = nData3;

	return nMax;
}

void PrintData(int nData1, int nData2, int nData3, int nMax)
{
	printf("%d, %d, %d 중 가장 큰 수는 %d 입니다.\n", nData1, nData2, nData3, nMax);
}

int main(void)
{
	int aList[3] = { 0 };
	int nMax = -9999, i = 0;
	for (i = 0; i < 3; ++i)
		aList[i] = GetData();
	// 최댓값 계산
	nMax = GetMax(aList[0], aList[1], aList[2]);

	// 출력
	PrintData(aList[0], aList[1], aList[2], nMax);
	return 0;
}
/*
int main(void)
{
	int aList[3] = { 0 };
	int nMax = -9999, i = 0;
	// 입력
	for (i = 0; i < 3; ++i)
	{
		printf("정수를 입력하세요. : ");
		scanf("%d", &aList[i]);
	}

	// 최댓값 계산
	nMax = aList[0];
	for (i = 1; i < 3; ++i)
		if (aList[i] > nMax)
			nMax = aList[i];

	// 출력
	printf("%d, %d, %d 중 가장 큰 수는 %d 입니다.\n",
		aList[0], aList[1], aList[2], nMax);
	return 0;
}
*/