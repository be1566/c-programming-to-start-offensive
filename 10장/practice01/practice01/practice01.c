#include <stdio.h>

int GetMin(int *, int);

int main(void)
{
	int aList[4] = { 0 };
	int nResult = 0;

	for (int i = 0; i < 4; ++i)
		scanf("%d", &aList[i]);

	nResult = GetMin(aList, 4);
	printf("%d\n", nResult);

	return 0;
}

int GetMin(int *pList, int nSize)
{
	int nMin = pList[0];

	for (int i = 0; i < nSize - 1; ++i)
		for (int k = i + 1; k < nSize; ++k)
			if (pList[k] < pList[i])
				nMin = pList[k];
	
	return nMin;
}