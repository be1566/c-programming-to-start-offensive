#include <stdio.h>

int GetFee(int, int);

int main(void)
{
	int nFee = 0, nAge = 0, nResult = 0;

	printf("기본요금을 입력하세요. : ");
	scanf("%d", &nFee);

	printf("나이를 입력하세요. : ");
	scanf("%d", &nAge);

	if (nAge < 1) nAge = 1;

	nResult = GetFee(nFee, nAge);

	printf("최종요금 : %d원 입니다.\n", nResult);

	return 0;
}

int GetFee(int nFee, int nAge)
{
	double dRate[20] = {
		0, 0, 0,
		0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
		0.75, 0.75, 0.75, 0.75, 0.75, 0.75
	};

	if (nAge > 20)
		return nFee;
	else
		return (int)(nFee * dRate[nAge - 1]);
}