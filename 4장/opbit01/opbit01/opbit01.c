#include <stdio.h>

int main(void)
{
	int nData = 0x11223344;

	printf("%08X\n", nData & 0x00FFFF00);	// 0x11223344 & 0x00FFFF00 -> 0x00223300
	printf("%08X\n", nData | 0x2211FFFF);	// 0x11223344 | 0x2211FFFF -> 0x3333FFFF
	printf("%08X\n", nData ^ 0x2211FFFF);	// 0x11223344 ^ 0x2211FFFF -> 0x3333CCBB
	printf("%08X\n", ~nData);				// 0xEEDDCCBB
	printf("%08X\n", nData >> 8);			// 0x00112233
	printf("%08X\n", nData << 16);			// 0x33440000
	return 0;
}