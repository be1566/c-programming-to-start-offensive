#include <stdio.h>

int main(void)
{
	// nTmp는 '교환을 위해 필요한 변수이다.
	int x = 10, y = 20, nTmp = 0;
	printf("Before: %d, %d\n", x, y);

	// 두 변수 x와 y에 저장된 값을 서로 교환한다.
	nTmp = x;
	x = y;
	y = nTmp;

	// 교환이 완료된 것을 확인하기 위해 출력한다.
	printf("After: %d, %d\n", x, y);

	/* XOR를 이용해서 nTmp 변수 선언 없이 교환한다.
	x ^= y;
	y ^= x;
	x ^= y;

	printf("New After: %d, %d\n", x, y);
	*/
	return 0;
}