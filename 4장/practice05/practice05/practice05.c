#include <stdio.h>

int main(void)
{
	/*
	int nInput;

	scanf("%d", &nInput);
	printf("%d %% 5 = %d\n", nInput, nInput % 5);
	*/
	printf("%08X\n", 0xFFFFFFFF & 0x00080000);
	printf("%08X\n", 0xAAAABBBB & 0x00080000);
	printf("%08X\n", 0xAABBCCDD & 0x00080000);

	printf("%08X\n", 0xFFFFFFFF | 0x00080000);
	printf("%08X\n", 0x11223344 | 0x00080000);
	printf("%08X\n", 0x00000000 | 0x000F0000);
	return 0;
}