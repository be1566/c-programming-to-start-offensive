#include <stdio.h>

int main(void)
{
	double dData1, dData2;

	printf("두 정수를 입력하세요.: ");
	scanf("%lf %lf", &dData1, &dData2);

	printf("AVG : %.2f", (dData1 + dData2) / 2);
	return 0;
}