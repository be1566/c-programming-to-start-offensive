// 메모리 복사 예제
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
	char szSrcBuf[12] = { "Hello" };
	char szDstBuf[12] = { 0 };

	// char *pszData = NULL;
	// 2번째 문제 주소가 바뀌어 버린다 메모리 누수 발생
	char *pszData = malloc(sizeof(char) * 12);

	// Deep copy !!! -> 내용을 복사
	memcpy(szDstBuf, szSrcBuf, sizeof(szDstBuf));	// 1번 문제 해결

	// Shallow copy !!! -> 주소만 복사
//	pszData = szSrcBuf;		// 1번 문제
	// Deep copy - 2번째 문제 해결
	memcpy(pszData, szSrcBuf, sizeof(char) * 12);

	puts(pszData);
	free(pszData);	// 2번째 문제 - free를 호출해도 당연히 문제가 발생 스택 부분을 가리켜 버렸음
	return 0;
}