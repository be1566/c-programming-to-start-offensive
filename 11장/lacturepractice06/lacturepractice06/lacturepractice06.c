// 다차원 배열에 대한 포인터
#include <stdio.h>

void PrintUser(char(*pUser)[12])
{
	for (int i = 0; i < 3; ++i)
		puts(pUser[i]);
}

int main(void)
{
	char aListUser[3][12] = {
		"철수",
		"길동",
		"영희"
	};

	PrintUser(aListUser);

	return 0;
}