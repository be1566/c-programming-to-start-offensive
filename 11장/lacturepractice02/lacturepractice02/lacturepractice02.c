#include <stdio.h>

int main(void)
{
	int aList[5] = { 40, 20, 50, 30, 10 };
	int *paList = aList;

	paList + 1;			// 기준 주소 + 정수(옵셋) -> 상대 주소
	*(paList + 1);		// int형 변수로 지정된다.

	*(paList + 1) = 5;
	paList[1] = 5;		// 11번째 줄과 같은 의미
}