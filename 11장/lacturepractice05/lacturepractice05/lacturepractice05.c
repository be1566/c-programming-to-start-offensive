#include <stdio.h>

int main(void)
{
	int nData = 10;

	// *(int*)0x0018FF00 주어진 Stack 영역이 맞다면 ...(즉, 사용 가능한 공간이 맞다면)
	*(int*)0x0018FF00 = 10;
	printf("%d\n", *(int*)0x0018FF00);
	
	return 0;
}