#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
	int nCount = 0, nSize = 0;

	printf("몇 번 문자열을 입력하시겠습니까? : ");
	scanf_s("%d", &nCount);

	printf("입력할 문자열의 길이를 입력하세요 : ");
	scanf_s("%d", &nSize);

	char **ppstrList = (char**)malloc(sizeof(char *) * nCount);
	for (int i = 0; i < nCount; ++i)
	{
		*(ppstrList + i) = (char*)malloc(nSize + 1);
		getchar('\n');
		gets_s(*(ppstrList + i), nSize + 1);
	}

	for (int i = 0; i < nCount; ++i)
		printf("%s\n", *(ppstrList + i));

	for (int i = 0; i < nCount; ++i)
		free(*(ppstrList + i));

	free(ppstrList);
	return 0;
}