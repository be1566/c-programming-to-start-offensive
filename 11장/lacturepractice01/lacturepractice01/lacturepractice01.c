#include <stdio.h>

int main(void)
{
	int nData = 300;
	int *pnData = &nData;

	*((int*)0x0018FF28) = 600;		// 직접 지정
	*pnData = 600;					// 간접 지정

	return 0;
}