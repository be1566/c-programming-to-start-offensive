#include <stdio.h>
#include <string.h>

void GetName(char *pszName, int nSize)		// Write 할때는 반드시 size를 넘겨주어야 한다.
{
	printf("Input your name: ");

	char szBuffer[32];
	gets_s(szBuffer, sizeof(szBuffer));
	strcpy_s(pszName, nSize, szBuffer);
}

void PrintName(const char *pszName)		// Read 만 하기 때문에 수정 못하게 해야 한다.
{
	printf("Your name is %s.\n", pszName);
//	*pszName = 'A';
//	strcpy_s(pszName, 2, "A");	// 컴파일 타임에 오류를 알려준다. 이는 엄청 중요하다
}

int main(void)
{
	char szName[32] = { 0 };
	GetName(szName, sizeof(szName));
	return 0;
}