#include <stdio.h>

int main(void)
{
	int nTotal = 0;

	int i = 1;
	while (i <= 10)
	{
		nTotal += i;
		i++;
	}

	printf("Total : %d\n", nTotal);
	return 0;
}