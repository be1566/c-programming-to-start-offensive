#include <stdio.h>

int main(void)
{
	int nCount = 0;
	for (int i = 1; i <= 100; i++) {
		if (i % 4 == 0)
			nCount++;
	}
	printf("%d\n", nCount);
	return 0;
}