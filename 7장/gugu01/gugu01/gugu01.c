#include <stdio.h>

int main(void)
{
	int nInput;

	scanf("%d", &nInput);

	if (nInput >= 2 && nInput <= 9)
	{
		int i = 1;
		while (i <= 9)
		{
			printf("%d * %d = %d\n", nInput, i, nInput * i);
			i++;
		}
	}
	else
		printf("ERROR\n");

	return 0;
}