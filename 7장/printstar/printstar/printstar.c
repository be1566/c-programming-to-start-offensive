#include <stdio.h>

int main(void)
{
	int nInput;

	scanf("%d", &nInput);

	if (nInput < 1)
		nInput = 1;
	else if (nInput > 9)
		nInput = 9;

	int i = 0;
	while (i < nInput)
	{
		printf("*");
		i++;
	}

	putchar('\n');
	return 0;
}