#include <stdio.h>

int main(void)
{
	for (int i = 0; i < 5; i++) {
		for (int k = 0; k < 5 + i; k++) {
			if (i + k >= 4 && (i + k) % 2 == 0)
				printf("*\t");
			else
				putchar('\t');
		}
		putchar('\n');
	}
	return 0;
}