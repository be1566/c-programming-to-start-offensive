#include <stdio.h>

int main(void)
{
	/*
	i = 0				i = 1			i = 2			i = 3				i = 4
	k = 1, 2, 3, 4		k = 1, 2, 3		k = 1, 2		k = 1
	k = 5				k = 5, 4		k = 5, 4, 3		k = 5, 4, 3, 2		k = 5, 4, 3, 2, 1
	*/
	for (int i = 0; i < 5; i++) {
		for (int k = 0; k < 5; k++) {
			if (i + k >= 4)
				printf("*\t");
			else
				printf("\t");
		}
		putchar('\n');
	}
	return 0;
}